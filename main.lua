function beginContact(a, b, coll)
  local a_data = a:getUserData()
  local b_data = b:getUserData()
  if a_data.type == "bullet" and b_data.type == "enemy" then
    local hit = {bullet=a_data.index, enemy=b_data.index}
    table.insert(hits, hit)
  elseif b_data.type == "bullet" and a_data.type == "enemy" then
    local hit = {bullet=b_data.index, enemy=a_data.index}
    table.insert(hits, hit)
  elseif a_data.type == "gun" or b_data.type == "gun" then
    dead = true
  end
end

function endContact(a, b, coll)
  
end

function preSolve(a, b, coll)
  
end

function postSolve(a, b, coll, normalimpulse, tangentimpulse)
  
end

function love.load()
  math.randomseed(os.time())
  next_enemy = love.timer.getTime() + math.random() * 2
  width = 650
  height = 650
  love.physics.setMeter(64) --the height of a meter our worlds will be 64px
  world = love.physics.newWorld(0, 0, true) --create a world for the bodies to exist in with horizontal gravity of 0 and vertical gravity of 9.81
  world:setCallbacks(beginContact, endContact, preSolve, postSolve)
  objects = {} -- table to hold all our physical objects
  bulletID = 1
  bullets = {}
  enemyID = 1
  enemies = {}
  hits = {}
  dead = false
  --let's create the ground
  -- objects.ground = {}
  -- objects.ground.body = love.physics.newBody(world, 650/2, 650-50/2) --remember, the shape (the rectangle we create next) anchors to the body from its center, so we have to move it to (650/2, 650-50/2)
  -- objects.ground.shape = love.physics.newRectangleShape(650, 50) --make a rectangle with a width of 650 and a height of 50
  -- objects.ground.fixture = love.physics.newFixture(objects.ground.body, objects.ground.shape) --attach shape to body

  gunHeight = 20
  gunBaseWidth = 10
  gunCenterHeight = 6
  gunX = width / 2
  gunY = height / 2
  -- gunH = 0.5 * math.pi
  local tipY = -14
  local tipX = 0
  -- gunAuxLen = math.sqrt(gunCenterHeight * gunCenterHeight + (gunBaseWidth / 2) * (gunBaseWidth / 2))
  -- gunAuxAngle = math.atan(gunBaseWidth / 2 / gunCenterHeight)
  -- auxAngle1 = gunAuxAngle - gunH
  leftY = 6
  leftX = -5

  -- auxAngle2 = gunAuxAngle + gunH
  rightY = 6
  rightX = 5

  objects.gun = {}
  objects.gun.body = love.physics.newBody(world, width/2, height/2, "dynamic")
  objects.gun.shape = love.physics.newPolygonShape(tipX, tipY, leftX, leftY, rightX, rightY)
  objects.gun.fixture = love.physics.newFixture(objects.gun.body, objects.gun.shape, 1)
  objects.gun.fixture:setCategory(1)
  objects.gun.fixture:setMask(1)
  objects.gun.fixture:setUserData({type="gun"})

  --initial graphics setup
  love.graphics.setBackgroundColor(0, 0, 0) --set the background color to a nice blue
  love.window.setMode(width, height) --set the window dimensions to 650 by 650 with no fullscreen, vsync on, and no antialiasing
end

function randomPolygon()
  local minRadius = 3
  local maxRadius = 30
  local steps = 6
  local angle = 0
  local ret = {}
  for i=1,steps do
    local radius = minRadius + math.random() * (maxRadius - minRadius)
    table.insert(ret, radius * math.sin(angle))
    table.insert(ret, radius * -math.cos(angle))
    local angleStep = (2*math.pi - angle) * 0.1  + (2*math.pi - angle) * 0.3 * math.random()
    angle = angle + angleStep
  end
  return ret
end

function love.update(dt)
  if dead then
    return
  end
  world:update(dt) --this puts the world into motion

  for _, hit in pairs(hits) do
    local bullet = bullets[hit.bullet]
    if bullet == nil then
      goto continue
    end
    bullet.fixture:destroy()
    bullet.body:destroy()
    bullets[hit.bullet] = nil

    local enemy = enemies[hit.enemy]
    if enemy == nil then
      goto continue
    end
    enemy.life = enemy.life - 1
    if enemy.life <= 0 then
      enemy.fixture:destroy()
      enemy.body:destroy()
      enemies[hit.enemy] = nil
    end
    ::continue::
  end

  if love.timer.getTime() > next_enemy then
    local enemyY = height * math.random()
    local enemy = {}
    enemy.body = love.physics.newBody(world, 0, enemyY, "dynamic")
    enemy.shape = love.physics.newPolygonShape(unpack(randomPolygon()))
    enemy.life = math.random(1, 5)
    -- if math.random() < 0.8 then
    --   enemy.shape = love.physics.newCircleShape(5)
    --   enemy.life = 1
    -- else
    --   enemy.shape = love.physics.newCircleShape(15)
    --   enemy.life = 3
    -- end
    enemy.fixture = love.physics.newFixture(enemy.body, enemy.shape, 1)
    enemy.fixture:setCategory(2)
    enemy.body:setLinearVelocity(60 + 80 * math.random(), -50 + 100  * math.random())
    enemy.body:setAngularVelocity(2 + 6  * math.random())
    table.insert(enemies, enemyID, enemy)
    enemy.fixture:setUserData({type="enemy", index=enemyID})
    enemyID = enemyID + 1
    next_enemy = love.timer.getTime() + math.random() * 2
  end

  local angular_vel = objects.gun.body:getAngularVelocity()
  if love.keyboard.isDown("left") then
    -- objects.gun.body:setAngularVelocity(-8)
    if angular_vel > -20 then
      objects.gun.body:applyTorque(-10)
    end
  elseif love.keyboard.isDown("right") then
    -- objects.gun.body:setAngularVelocity(8)
    if angular_vel < 20 then
      objects.gun.body:applyTorque(10)
    end
  else
    objects.gun.body:applyTorque(-angular_vel * 4)
  end

  local angle = objects.gun.body:getAngle()
  local xvel, yvel = objects.gun.body:getLinearVelocity()
  local mag = math.sqrt(xvel * xvel + yvel * yvel)
  -- local projected_vel = 0
  -- if mag > 0 then
  --   local vel_angle = math.asin(xvel / mag)
  --   projected_vel = mag * math.cos(angle - vel_angle)
  -- end
  projected_vel = xvel * math.sin(angle) - yvel * math.cos(angle)
  -- local xvel = projected_vel * math.sin(angle)
  -- local yvel = projected_vel * -math.cos(angle)
  -- objects.gun.body:setLinearVelocity(xvel, yvel)

  local max_vel = 200
  local force = 200
  if love.keyboard.isDown("up") then
    if mag < max_vel then
      objects.gun.body:applyForce(force * math.sin(angle), -force * math.cos(angle))
    end
  elseif love.keyboard.isDown("down") then
    if mag < max_vel then
      objects.gun.body:applyForce(-force * math.sin(angle), force * math.cos(angle))
    end
  else
    objects.gun.body:applyForce(-xvel / 10, -yvel / 10)
    -- objects.gun.body:setLinearVelocity(0, 0)
  end

  -- if mag > max_vel then
  --   xvel = max_vel * math.sin(angle)
  --   yvel = max_vel * math.sin(angle)
  --   objects.gun.body:setLinearVelocity(xvel, yvel)
  -- end

  local x, y = objects.gun.body:getPosition()
  local wrap = false
  if x < 0 then
    x = x + width
    wrap = true
  elseif x > width then
    x = x - width
    wrap = true
  end

  if y < 0 then
    y = y + height
    wrap = true
  elseif y > height then
    y = y - height
    wrap = true
  end
  if wrap then
    objects.gun.body:setPosition(x, y)
  end

  local gone_bullets = {}
  for key, b in pairs(bullets) do
    local x, y = b.body:getPosition()
    if x < 0 or x > width or y < 0 or y > width then
      table.insert(gone_bullets, key)
    end
  end

  for _, value in pairs(gone_bullets) do
    bullets[value].fixture:destroy()
    bullets[value].body:destroy()
    bullets[value] = nil
  end

  local gone_enemies = {}
  for key, b in pairs(enemies) do
    local x, y = b.body:getPosition()
    if x < 0 or x > width or y < 0 or y > width then
      table.insert(gone_enemies, key)
    end
  end

  for _, value in pairs(gone_enemies) do
    enemies[value].fixture:destroy()
    enemies[value].body:destroy()
    enemies[value] = nil
  end
end

function love.draw()
  love.graphics.setColor(0.9, 0.9, 0.9) -- set the drawing color to green for the ground
  -- love.graphics.polygon("fill", objects.ground.body:getWorldPoints(objects.ground.shape:getPoints())) -- draw a "filled in" polygon using the ground's coordinates
  love.graphics.polygon("fill", objects.gun.body:getWorldPoints(objects.gun.shape:getPoints()))
  for _, b in pairs(bullets) do
    love.graphics.circle("fill", b.body:getX(), b.body:getY(), b.shape:getRadius())
  end
  for _, b in pairs(enemies) do
    -- love.graphics.circle("line", b.body:getX(), b.body:getY(), b.shape:getRadius())
    love.graphics.polygon("line", b.body:getWorldPoints(b.shape:getPoints()))
    local x, y = b.body:getPosition()
    love.graphics.print(b.life, x, y)
  end
  love.graphics.print("projected_vel:" .. projected_vel, 0, 0)
  if dead then
    love.graphics.print("dead", width / 2, height / 2)
  end
end

function love.keypressed(key)
  if key == 'space' then
    local bullet = {}
    local x, y = objects.gun.body:getPosition()
    local angle = objects.gun.body:getAngle()
    local tipY = y - (gunHeight - gunCenterHeight) * math.cos(angle)
    local tipX = x + (gunHeight - gunCenterHeight) * math.sin(angle)

    local n = 101
    local mid = math.ceil(n/2)
    for i = 1,n do
      local bullet = {}
      bullet.body = love.physics.newBody(world, tipX, tipY, "dynamic")
      bullet.shape = love.physics.newCircleShape(3)
      bullet.fixture = love.physics.newFixture(bullet.body, bullet.shape, 1)
      bullet.fixture:setCategory(1)
      bullet.fixture:setMask(1)
      bullet.body:setLinearVelocity(400 * math.sin(angle - (i - mid)/36 * math.pi), -400 * math.cos(angle - (i-mid)/36*math.pi))
      table.insert(bullets, bulletID, bullet)
      bullet.fixture:setUserData({type="bullet", index=bulletID})
      bulletID = bulletID + 1
    end
    -- bullet.body = love.physics.newBody(world, tipX, tipY, "dynamic")
    -- bullet.shape = love.physics.newCircleShape(3)
    -- bullet.fixture = love.physics.newFixture(bullet.body, bullet.shape, 1)
    -- bullet.fixture:setCategory(1)
    -- bullet.fixture:setMask(1)
    -- bullet.body:setLinearVelocity(400 * math.sin(angle), -400 * math.cos(angle))
    -- table.insert(bullets, bulletID, bullet)
    -- bullet.fixture:setUserData({type="bullet", index=bulletID})
    -- bulletID = bulletID + 1

    -- local bullet2 = {}
    -- bullet2.body = love.physics.newBody(world, tipX, tipY, "dynamic")
    -- bullet2.shape = love.physics.newCircleShape(3)
    -- bullet2.fixture = love.physics.newFixture(bullet2.body, bullet2.shape, 1)
    -- bullet2.fixture:setCategory(1)
    -- bullet2.fixture:setMask(1)
    -- bullet2.body:setLinearVelocity(400 * math.sin(angle + math.pi/36), -400 * math.cos(angle + math.pi/36))
    -- table.insert(bullets, bulletID, bullet2)
    -- bullet2.fixture:setUserData({type="bullet", index=bulletID})
    -- bulletID = bulletID + 1

    -- local bullet3 = {}
    -- bullet3.body = love.physics.newBody(world, tipX, tipY, "dynamic")
    -- bullet3.shape = love.physics.newCircleShape(3)
    -- bullet3.fixture = love.physics.newFixture(bullet3.body, bullet3.shape, 1)
    -- bullet3.fixture:setCategory(1)
    -- bullet3.fixture:setMask(1)
    -- bullet3.body:setLinearVelocity(400 * math.sin(angle - math.pi/36), -400 * math.cos(angle - math.pi/36))
    -- table.insert(bullets, bulletID, bullet3)
    -- bullet3.fixture:setUserData({type="bullet", index=bulletID})
    -- bulletID = bulletID + 1
  end
end
